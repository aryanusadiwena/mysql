MariaDB [(none)]> create database myshop
use myshop
	MariaDB [myshop]>create table users(
    -> id INT(10) AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
	Query OK, 0 rows affected (0.029 sec)
	
	MariaDB [myshop]>create table kategori(
    -> id INT(10) AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255)
    -> );
	Query OK, 0 rows affected (0.026 sec)

	MariaDB [myshop]>create table item(
    -> id INT(10) AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(255),
    -> stock int(100),
    -> kategori_id int(10),
    -> foreign key(kategori_id) references kategori(id)
    -> );
	Query OK, 0 rows affected (0.032 sec)
	
	MariaDB [myshop]> describe item;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(10)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(255)     | YES  |     | NULL    |                |
| stock       | int(100)     | YES  |     | NULL    |                |
| kategori_id | int(10)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.015 sec)
	
	MariaDB [myshop]> insert into kategori(name) values("gadget"),("cloth"),("men"),("women"),("branded");
	
	MariaDB [myshop]> select * from item;
Empty set (0.001 sec)

MariaDB [myshop]> select * from kategori;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

MariaDB [myshop]> insert into users(name,email,password)
    -> values("John Doe","john@doe.com","jenita123");
Query OK, 1 row affected (0.002 sec)

MariaDB [myshop]> insert into users(name,email,password)
    -> values("Jane Doe","jane@doe.com","jenita123");
Query OK, 1 row affected (0.003 sec)

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | jenita123 |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.000 sec)

MariaDB [myshop]> insert into item(name,description,price,stock,kategori_id)
    -> values("Samsung b50","hape keren dari merek sumsang","40","100","1"),
    -> ("Uniklooh","baju keren dari brand ternama","50","50","2"),
    -> ("IMHO Watch","jam tangan anak yang jujur banget","20","10","1");
Query OK, 3 rows affected (0.002 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from item;
+----+-------------+-----------------------------------+-------+-------+-------------+
| id | name        | description                       | price | stock | kategori_id |
+----+-------------+-----------------------------------+-------+-------+-------------+
|  1 | Samsung b50 | hape keren dari merek sumsang     |    40 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |    50 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget |    20 |    10 |           1 |
+----+-------------+-----------------------------------+-------+-------+-------------+
3 rows in set (0.000 sec)

MariaDB [myshop]> update item set price=25 where id=1;
Query OK, 1 row affected (0.002 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from item;
+----+-------------+-----------------------------------+-------+-------+-------------+
| id | name        | description                       | price | stock | kategori_id |
+----+-------------+-----------------------------------+-------+-------+-------------+
|  1 | Samsung b50 | hape keren dari merek sumsang     |    25 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |    50 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget |    20 |    10 |           1 |
+----+-------------+-----------------------------------+-------+-------+-------------+
3 rows in set (0.001 sec)

